import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';
import { User } from '../models/user.model';
import Config from "../config";
import { UserService } from '../users/user.service';

@Injectable()
export class AuthService {
	onLogout: EventEmitter<User> = new EventEmitter();
	onLogin: EventEmitter<User> = new EventEmitter();
	onUserFetched: EventEmitter<User> = new EventEmitter();

	user:User;
	userId: string;

	constructor(private http: HttpClient, public jwtHelper: JwtHelperService, private userService:UserService) {
		if (localStorage.getItem("authKey") == null){
			localStorage.setItem("authKey", "");
		}
    }
    
    getAuthorizationToken(): string {
        return localStorage.getItem("authKey");
	}
	
	isAuthenticated(): boolean {
		const authKey = localStorage.getItem('authKey');
		return !this.jwtHelper.isTokenExpired(authKey);
	}

	isLoggedIn(): boolean {
		if (localStorage.getItem("authKey")){
		  return true;
		}
		return false;
	}
	
	isAdmin(): boolean {
		if (localStorage.getItem("isAdmin")){
		  return this.isLoggedIn();
		}
		return false;
	}

	logout(): any {
		localStorage.clear();
		localStorage.setItem("authKey", "");
		this.onLogout.emit();
	}
	
	async getCurrentUser(): Promise<User> {
		this.userId = localStorage.getItem("userId");
		if (this.userId != null){
			let result = await this.http.get(Config.basesite + "/users/" + this.userId).toPromise();
			this.user = new User(result);
			return this.user;
		}
	}
	
	async login(email: string, password: string): Promise<boolean> {
		try{
			let loginResult: any = await this.http.post(Config.basesite + "/auth/login", {email, password}).toPromise();
			localStorage.setItem("authKey", loginResult.token);
			localStorage.setItem("userId", loginResult.userId);

			this.user = await this.userService.getUser(loginResult.userId);
			if(this.user.isAdmin){
				localStorage.setItem("isAdmin", "yes");
			}

			this.onLogin.emit(this.user);
			return true;

		}catch(err){
		  	console.log(err);
		  	return false;
		}
	}
}
