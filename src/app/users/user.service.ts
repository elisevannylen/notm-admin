import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user.model';
import Config from '../config';
import { Post } from '../models/post.model';

@Injectable()
export class UserService{
    users: User[];
	user: User;
	userId: string;

	constructor(private http: HttpClient) { }

    async getUsers():Promise<Array<User>>{
        let results = await this.http.get(Config.basesite + "/users").toPromise();

        let users:Array<User> = [];
        for(let i in results){
            let user = new User(results[i]);
            users.push(user);
        }

        return users;
    }

    async getUsageUserInPosts(userId:string):Promise<number>{
        let results = await this.http.get(Config.basesite + "/posts/covers").toPromise();
        let usage: number = 0;
        
		for (let i in results){
            let post = new Post(results[i]);
            if(post.details.creator._id == userId){
                usage++;
            }
		}
		return usage;
	}

    async getUser(userId:string):Promise<User>{
        let result = await this.http.get(Config.basesite + "/users/" + userId).toPromise();
        return new User(result);
    }

    async createUser(user:User):Promise<User>{
        let result = await this.http.post(Config.basesite + "/users", user).toPromise();
        return new User(result);
    }

    async updateUser(user:User):Promise<User>{
        let result = await this.http.put(Config.basesite + "/users/" + user._id, user).toPromise();
        return new User(result);
    }

    async deleteUser(userId:string):Promise<any>{
        let result:any = await this.http.delete(Config.basesite + "/users/" + userId).toPromise();
    }
}