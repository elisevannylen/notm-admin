import { Component, OnInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { UserService } from '../user.service';
import { User } from '../../models/user.model';
import { NotificationHelper } from '../../helpers/notificationHelper';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  	selector: 'app-user-list',
  	templateUrl: './user-list.component.html',
  	styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit, OnDestroy {
	mobileQuery: MediaQueryList;
	displayedColumns = ['username', 'email', 'fullName', 'isAdmin', 'actions'];
	users: User[] = [];
	private _mobileQueryListener: () => void;

	constructor(private userService:UserService, private notificationHelper:NotificationHelper, private authService:AuthService,
		changeDetectorRef:ChangeDetectorRef, media:MediaMatcher) { 
			this.mobileQuery = media.matchMedia('(max-width: 600px)');
			this._mobileQueryListener = () => changeDetectorRef.detectChanges();
			this.mobileQuery.addListener(this._mobileQueryListener);
	}

	async ngOnInit() {
		await this.getUsers();
		this.displayedColumns = (this.mobileQuery.matches) 
			? ['username', 'email', 'actions'] 
			: ['username', 'email', 'fullName', 'isAdmin', 'actions'];
	}

	ngOnDestroy(): void {
		this.mobileQuery.removeListener(this._mobileQueryListener);
	}

	async getUsers(){
		this.users = await this.userService.getUsers();
	}

	async onDelete(userId:string):Promise<void> {
		let usageInPosts = await this.userService.getUsageUserInPosts(userId);
		let user = await this.userService.getUser(userId);
		let currentUser = await this.authService.getCurrentUser();

		if(currentUser._id != userId){
			if(this.users.length != 1){
				if(usageInPosts == 0){
					//await this.userService.deleteUser(userId);
					await this.getUsers();
					this.notificationHelper.openUndoSnackBar("User '" + user.username + "' is deleted.", async () => {
						await this.userService.createUser(user);
						await this.getUsers();
					});
				} else {
					this.notificationHelper.openSnackBar("User '" + user.username + "' is used " + usageInPosts + " times.", "Ok");
				}
			} else {
				this.notificationHelper.openSnackBar("You can't delete the last user.", "Ok");
			}
		} else {
			this.notificationHelper.openSnackBar("You can't delete your own user.", "Ok");
		}	
	}
}
