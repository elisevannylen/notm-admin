import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../user.service';
import { User } from '../../models/user.model';
import { NotificationHelper } from '../../helpers/notificationHelper';

@Component({
	selector: 'app-user-detail',
	templateUrl: './user-detail.component.html',
	styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit, OnDestroy {
	private sub: any;
	userId: string;
	user:User = new User();
	title: string = "Add User";
	passwordRequired: boolean = true;
	wrongEmailFormat: boolean = false;

	constructor(private router: Router, private route: ActivatedRoute, private userService:UserService, private notificationHelper:NotificationHelper) { }

	async ngOnInit(): Promise<void> {
		this.sub = this.route.params.subscribe(params => {this.userId = params['id'];});
		if(this.userId != "0"){
			this.user = await this.userService.getUser(this.userId);
			this.passwordRequired = false;
			this.user.password = null;
			this.title = "Update user";
		}
	}

	ngOnDestroy(): void {
    	this.sub.unsubscribe();
	}

	async onSaveUser(){
		if(this.isEmailFormat(this.user.email)){
			try{
				if(this.userId == "0"){
					await this.userService.createUser(this.user);
				} else {
					await this.userService.updateUser(this.user);
				}
				
				this.router.navigate(['/users']);
			} catch (err) {
				if(this.isValidationErrorOnEmail(err.error.error.message)){
					this.notificationHelper.openSnackBar("E-mail address already exists, it must be unique.", "", 3000);
				}
			}
		} else {
			this.wrongEmailFormat = true;
		}
	}

	private isEmailFormat(email:string): boolean {
		let hasRightFormat: boolean = false;
		let emailRegex = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
		
		hasRightFormat = emailRegex.test(email);

		return hasRightFormat;
	}

	private isValidationErrorOnEmail(message:any): boolean{
		return message.includes("email") && message.includes("validation") && message.includes("unique");
	}
}
