import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaterialModule } from '../material-module';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PostsRoutingModule } from './posts-routing.module';

import { PostService } from './post.service';
import { TagService } from '../tags/tag.service';

import { PostListComponent } from './post-list/post-list.component';
import { PostDetailComponent } from './post-detail/post-detail.component';
import { PostCardComponent } from './post-card/post-card.component';
import { PostDetailTagsComponent } from './post-detail-tags/post-detail-tags.component';
import { PostDetailPicturesComponent } from './post-detail-pictures/post-detail-pictures.component';
import { PostDetailShowComponent } from './post-detail-show/post-detail-show.component';

@NgModule({
  declarations: [
    PostCardComponent,
    PostListComponent, 
    PostDetailComponent, 
    PostDetailTagsComponent, 
    PostDetailPicturesComponent, PostDetailShowComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    AngularFontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    PostsRoutingModule
  ],
  providers: [
    PostService,
    TagService
  ]
})
export class PostsModule { }
