import { Component, OnInit, Input, Output, ViewChild, EventEmitter, ElementRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatAutocomplete, MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Tag } from '../../models/tag.model';
import { TagService } from '../../tags/tag.service';

@Component({
	selector: 'app-post-detail-tags',
	templateUrl: './post-detail-tags.component.html',
	styleUrls: ['./post-detail-tags.component.css']
})
export class PostDetailTagsComponent implements OnInit {
	visible = true;
	selectable = true;
	removable = true;
	separatorKeysCodes: number[] = [ENTER, COMMA];
	tagCtrl = new FormControl();
	filteredTags: Observable<string[]>;
	allTags: Tag[] = [];
	allStringTags: string[] = [];

	@Input() tags: Tag[] = [];

	@ViewChild('tagInput', {static: false}) tagInput: ElementRef<HTMLInputElement>;
	@ViewChild('auto', {static: false}) matAutocomplete: MatAutocomplete;

  	constructor(private tagService:TagService) { }

	async ngOnInit() {
		this.allTags = await this.tagService.getTags();

		for(let tag of this.allTags){
			this.allStringTags.push(tag.name);
		}

		this.filteredTags = this.tagCtrl.valueChanges.pipe(
			startWith(null),
			map((tag: string | null) => tag ? this._filter(tag) : this.allStringTags.slice()));
	}

	add(event: MatChipInputEvent): void {
		const input = event.input;
		const value = event.value;

		let tag = this.allTags.find(t => t.name === value.trim());

		if(tag && this._canAddTag(tag)){
			this.tags.push(tag);
		}
			
		if(input){
			input.value = '';
		}
	
		this.tagCtrl.setValue(null);
	}
	
	selected(event: MatAutocompleteSelectedEvent): void {
		let tag = this.allTags.find(t => t.name === event.option.viewValue);
		if(this._canAddTag(tag)){
			this.tags.push(tag);
		}
		this.tagInput.nativeElement.value = '';
		this.tagCtrl.setValue(null);
	}
	
	remove(tag: Tag): void {
		const index = this.tags.indexOf(tag);
	
		if (index >= 0) {
		  this.tags.splice(index, 1);
		}
	}
	
	private _filter(value: string): string[] {
		const filterValue = value.toLowerCase();
	
		return this.allStringTags.filter(tag => tag.toLowerCase().indexOf(filterValue) === 0);
	}

	private _canAddTag(tag: Tag): boolean{
		return this.tags.find(t => t.name.toLowerCase() === tag.name.toLowerCase()) == undefined;
	}
}
