import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { PostCover } from '../../models/postCover.model';

@Component({
  selector: 'app-post-card',
  templateUrl: './post-card.component.html',
  styleUrls: ['./post-card.component.css']
})
export class PostCardComponent implements OnInit {
	@Input() postCover: PostCover;
	@Output() deleteClicked = new EventEmitter();
  	writeDate: Date;

  	constructor() { }

	ngOnInit(): void {
		this.writeDate = (this.postCover.writeDate != null)? this.postCover.writeDate : this.postCover.creationDate;
	}

  	OnDeleteClicked(postId: string) {
		this.deleteClicked.emit(postId);
	}
}
