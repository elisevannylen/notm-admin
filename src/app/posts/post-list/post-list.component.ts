import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { PostCover } from '../../models/postCover.model';
import { PostService } from '../post.service';
import { NotificationHelper } from '../../helpers/notificationHelper';

@Component({
	selector: 'app-post-list',
	templateUrl: './post-list.component.html',
	styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit {
	postCovers:PostCover[];
	mobileQuery: MediaQueryList;
	tabletQuery: MediaQueryList;
	private _mobileQueryListener: () => void;
	
	constructor(private postService:PostService, private notificationHelper:NotificationHelper,
		changeDetectorRef:ChangeDetectorRef, media:MediaMatcher) { 
			this.mobileQuery = media.matchMedia('(max-width: 600px)');
			this.tabletQuery = media.matchMedia('(min-width: 601px) and (max-width: 1000px)')
			this._mobileQueryListener = () => changeDetectorRef.detectChanges();
			this.mobileQuery.addListener(this._mobileQueryListener);
			this.tabletQuery.addListener(this._mobileQueryListener);
	}

	async ngOnInit() {
		await this.getPostCovers();
	}

  	async getPostCovers(){
		try {
			this.postCovers = await this.postService.getPostCovers();
		} catch (err) {
			console.log(err);
		}
	}

	async onDelete(postId:string):Promise<void> {
		let post = await this.postService.getPost(postId);

		await this.postService.deletePost(postId);
		await this.getPostCovers();
		this.notificationHelper.openUndoSnackBar("Post '" + post.details.title + "' is deleted.", async () => {
			await this.postService.createPost(post);
			await this.getPostCovers();
		});
	}
}
