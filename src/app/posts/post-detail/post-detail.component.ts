import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PostService } from '../post.service';
import { Post } from '../../models/post.model';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.css']
})
export class PostDetailComponent implements OnInit, OnDestroy {
  	private sub: any;
	postId: string;
	post:Post = new Post();
	title: string = "Add post";

  	constructor(private router:Router, private route:ActivatedRoute, private postService:PostService, private authService:AuthService) { }

	async ngOnInit(): Promise<void> {
		this.sub = this.route.params.subscribe(params => {this.postId = params['id'];});
		if(this.postId != "0"){
			this.post = await this.postService.getPost(this.postId);
			this.title = "Update post";
		}
	}

	ngOnDestroy(): void {
		this.sub.unsubscribe();
	}

	async onSavePost(){
		this.post.details.creator = await this.authService.getCurrentUser();

		if(this.postId == "0"){
			await this.postService.createPost(this.post);
		} else {
			await this.postService.updatePost(this.post);
		}

		this.router.navigate(['/posts']);
	}

	addHTMLTagToText(htmlTag: string){
		this.post.text = this.post.text.concat(htmlTag);
	}
}
