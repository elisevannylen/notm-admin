import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Post } from '../models/post.model';
import { PostCover } from '../models/postCover.model';
import { Picture } from '../models/picture.model';
import { Tag } from '../models/tag.model';
import Config from "../config";

@Injectable()
export class PostService {
    constructor(private http: HttpClient) { }

    async getPosts():Promise<Array<Post>>{
		let results = await this.http.get(Config.basesite + "/posts").toPromise();

		let posts:Array<Post> = [];
		for (let i in results){
			let post = new Post(results[i]);
			posts.push(post);
		}
		posts.sort(function(a, b){
            let aDate = (a.details.writeDate != null) ? a.details.writeDate : a.details.creationDate;
            let bDate = (b.details.writeDate != null) ? b.details.writeDate : b.details.creationDate;
			return bDate.getTime() - aDate.getTime();
		});
		return posts;
    }
    
    async getPostCovers():Promise<Array<PostCover>>{
		let results = await this.http.get(Config.basesite + "/posts/covers").toPromise();
		
		let postCovers:Array<PostCover> = [];
		for (let i in results){
			let postCover = this.convertPostToPostCover(new Post(results[i]));
			postCovers.push(postCover);
		}
		postCovers.sort(function(a, b){
            let aDate = (a.writeDate != null) ? a.writeDate : a.creationDate;
            let bDate = (b.writeDate != null) ? b.writeDate : b.creationDate;
			return bDate.getTime() - aDate.getTime();
		});
		return postCovers;
    }
    
    async getPostsByTag(tagId:string):Promise<Array<Post>>{
		let results = await this.http.get(Config.basesite + "/posts/byTag/" + tagId).toPromise();

		let posts:Array<Post> = [];
		for (let i in results){
			let post = new Post(results[i]);
			posts.push(post);
		}
		posts.sort(function(a, b){
			let aDate = (a.details.writeDate != null) ? a.details.writeDate : a.details.creationDate;
            let bDate = (b.details.writeDate != null) ? b.details.writeDate : b.details.creationDate;
			return bDate.getTime() - aDate.getTime();
		});
		return posts;
	}
    
    async getPostCoversByTag(tagId:string):Promise<Array<PostCover>>{
		let results = await this.http.get(Config.basesite + "/posts/coversByTag/" + tagId).toPromise();

		let postCovers:Array<PostCover> = [];
		for (let i in results){
			let postCover = this.convertPostToPostCover(new Post(results[i]));
			postCovers.push(postCover);
		}
		postCovers.sort(function(a, b){
			let aDate = (a.writeDate != null) ? a.writeDate : a.creationDate;
            let bDate = (b.writeDate != null) ? b.writeDate : b.creationDate;
			return bDate.getTime() - aDate.getTime();
		});
		return postCovers;
	}

    async getPost(postId:string):Promise<Post>{
        let result = await this.http.get(Config.basesite + "/posts/" + postId).toPromise();
        return new Post(result);
    }

    async createPost(post:Post):Promise<Post>{
        let result = await this.http.post(Config.basesite + "/posts", post).toPromise();
        return new Post(result);
    }

    async updatePost(post:Post):Promise<Post>{
        let result = await this.http.put(Config.basesite + "/posts/" + post._id, post).toPromise();
        return new Post(result);
    }

    async deletePost(postId:string):Promise<any>{
        let result = await this.http.delete(Config.basesite + "/posts/" + postId).toPromise();
    }

    async getFeaturePicture(postId:string):Promise<Picture>{
        let result = await this.http.get(Config.basesite + "/posts/" + postId + "/featurepicture").toPromise();
        let post = new Post(result);
        return new Picture(post.pictures[0]);
	}
	
	private convertPostToPostCover(post:Post): PostCover{
		let postCover = new PostCover(post.details);
		postCover.postId = post._id;
		postCover.tags = post.tags.map(function(tag:any){ return new Tag(tag); });
		return postCover;
	}
}