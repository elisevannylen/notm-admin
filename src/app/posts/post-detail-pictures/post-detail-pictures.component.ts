import { Component, Input } from '@angular/core';
import { Picture } from '../../models/picture.model';

@Component({
	selector: 'app-post-detail-pictures',
	templateUrl: './post-detail-pictures.component.html',
	styleUrls: ['./post-detail-pictures.component.css']
})
export class PostDetailPicturesComponent {
	selectedPicture: Picture = new Picture();
	editMode: boolean = false;

	@Input() pictures: Picture[] = [];

	constructor() { }

	addOrEditPicture(){
		if(!this._isNullOrEmpty(this.selectedPicture.description) && !this._isNullOrEmpty(this.selectedPicture.location)){
			this._resetFeaturePictureBooleansIfSelectedPictureIsFeaturePicture();

			if(!this.editMode){
				this.pictures.push(this.selectedPicture);
			}

			this.selectedPicture = new Picture();
			this.editMode = false;
		}
	}

	editPicture(picture:Picture){
		this.editMode = true;
		this.selectedPicture = picture;
	}

	removePicture(picture: Picture): void {
		const index = this.pictures.indexOf(picture);
	
		if (index >= 0) {
			this.pictures.splice(index, 1);
		}
	}

	private _resetFeaturePictureBooleansIfSelectedPictureIsFeaturePicture(){
		if(this.selectedPicture.isFeaturePicture == true){
			let otherFeaturePictures = this.pictures.filter(p => p.isFeaturePicture == true && !this._isSamePicture(p));
			for(let featurePicture of otherFeaturePictures){
				featurePicture.isFeaturePicture = false;
			}
		}
	}

	private _isSamePicture(picture: Picture){
		return picture.description == this.selectedPicture.description 
			&& picture.location == this.selectedPicture.location 
			&& picture.isFeaturePicture == this.selectedPicture.isFeaturePicture;
	}

	private _isNullOrEmpty(object:string): boolean {
		if(object == null || object == ""){
			  return true;
		}
		return false;
	}
}
