import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PostService } from '../post.service';
import { Post } from '../../models/post.model';

@Component({
	selector: 'app-post-detail-show',
	templateUrl: './post-detail-show.component.html',
	styleUrls: ['./post-detail-show.component.css']
})
export class PostDetailShowComponent implements OnInit, OnDestroy {
	private sub: any;
	postId: string;
	post:Post = new Post();

  	constructor(private route:ActivatedRoute, private postService:PostService) { }

	async ngOnInit(): Promise<void> {
		this.sub = this.route.params.subscribe(params => {this.postId = params['id'];});
		this.post = await this.postService.getPost(this.postId);
	}

	ngOnDestroy(): void {
		this.sub.unsubscribe();
	}

}
