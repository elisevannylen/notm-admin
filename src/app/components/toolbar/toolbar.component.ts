import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';
import { User } from 'src/app/models/user.model';
import { Router } from '@angular/router';

@Component({
	selector: 'app-toolbar',
	templateUrl: './toolbar.component.html',
	styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {
  	@Input() isMobile: boolean;
	@Output() menuClicked = new EventEmitter<any>();

	shortBlogTitle: string = "NotM - Admin";
	longBlogTitle: string = "Nerd on the Move - Admin";
	username: string;
	isLoggedId: boolean;

	constructor(private authService:AuthService, private router: Router) { 
		this.isLoggedId = authService.isLoggedIn();

		authService.onLogout.subscribe(() => this.isLoggedId = authService.isLoggedIn());
		authService.onLogin.subscribe(async () => {
			this.isLoggedId = authService.isLoggedIn();
			this.username = await this.getUsername();
		});
	}

	async ngOnInit() {
		this.username = await this.getUsername();
	}

	menuIconClicked() {
		this.menuClicked.emit();
	}

	logout() {
		this.authService.logout();
		this.router.navigate(['/login']);
	}

	async getUsername(): Promise<string>{
		let user = new User(await this.authService.getCurrentUser());
		return user.username;
	}
}
