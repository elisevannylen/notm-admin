import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { User } from '../../../models/user.model';
import { AuthService } from '../../../auth/auth.service';
import { NotificationHelper } from 'src/app/helpers/notificationHelper';

@Component({
  selector: 'app-login-dialog',
  templateUrl: './login-dialog.component.html',
  styleUrls: ['./login-dialog.component.css']
})
export class LoginDialogComponent {
	wrongEmailFormat: boolean = false;
	hidePassword = true;

  	constructor(private authService:AuthService, public dialogRef: MatDialogRef<LoginDialogComponent>,
    	@Inject(MAT_DIALOG_DATA) public data: User, private notificationHelper:NotificationHelper) {}

    async login() {
      if(!this.isNullOrEmpty(this.data.email) && !this.isNullOrEmpty(this.data.password)){
		if(this.isEmailFormat(this.data.email)){
			let signedIn = await this.authService.login(
				this.data.email,
				this.data.password
			);
	
			if(signedIn){
				console.log('Login successful!');
				this.dialogRef.close();
			}
			else {
				console.log('Something went wrong.');
				this.notificationHelper.openSnackBar("Incorrect login credentials.", "Ok");
			}
		} else {
			this.wrongEmailFormat = true;
		}
      }
	}

    private isNullOrEmpty(object:string): boolean {
      if(object == null || object == ""){
        	return true;
      }
      return false;
	}
	
	private isEmailFormat(email:string): boolean {
		let hasRightFormat: boolean = false;
		let emailRegex = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
		
		hasRightFormat = emailRegex.test(email);

		return hasRightFormat;
	}
}
