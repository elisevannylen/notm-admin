import { Component, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MediaMatcher } from '@angular/cdk/layout';
import { Router } from "@angular/router";
import { User } from '../../models/user.model';
import { AuthService } from '../../auth/auth.service';
import { LoginDialogComponent } from './login-dialog/login-dialog.component';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})
export class LoginComponent implements AfterViewInit {
	mobileQuery: MediaQueryList;
	user: User;

	private _mobileQueryListener: () => void;

	constructor(public dialog:MatDialog, private authService:AuthService, private router:Router, changeDetectorRef:ChangeDetectorRef, media:MediaMatcher) {
		this.mobileQuery = media.matchMedia('(max-width: 600px)');
		this._mobileQueryListener = () => changeDetectorRef.detectChanges();
		this.mobileQuery.addListener(this._mobileQueryListener);
	}
	
	public ngAfterViewInit(): void {
		if(!this.authService.isAuthenticated()) {
		this.openDialog();
		}
	}

	openDialog(): void {
		this.user = new User();
		const dialogRef = this.dialog.open(LoginDialogComponent, this.setDialogConfig());

		dialogRef.afterClosed().subscribe(() => {
			this.router.navigate(['/']);
		});
	}

	private setDialogConfig(): MatDialogConfig {
		const dialogConfig = new MatDialogConfig();

		dialogConfig.width = (this.mobileQuery.matches) ? '100vw' : '600px';
		dialogConfig.data = this.user;
		dialogConfig.disableClose = true;
		dialogConfig.autoFocus = true;
		dialogConfig.backdropClass = 'backdropBackground';

		return dialogConfig;
	}
}
