import * as autoBind from 'auto-bind';

export class Picture{
    _id: string;
    description: string;
    location: string;
    changeDate: Date = new Date();
    isFeaturePicture: boolean = false;

    constructor(data:any = null){
        autoBind(this);

        if(data != null){
            this._id = data._id;
            this.description = data.description;
            this.location = data.location;
            this.changeDate = new Date(data.changeDate);
            this.isFeaturePicture = data.isFeaturePicture;
        }
    }
}