import * as autoBind from 'auto-bind';

export class User{
    _id: string;
    username: string;
    email: string;
    password: string;
    fullName: string;
    isAdmin: boolean;

    constructor(data:any = null){
        autoBind(this);

        if(data != null){
            this._id = data._id;
            this.username = data.username;
            this.email = data.email;
            this.password = data.password;
            this.fullName = data.fullName;
            this.isAdmin = data.isAdmin;
        }
    }
}