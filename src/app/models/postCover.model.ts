import * as autoBind from 'auto-bind';
import { User } from './user.model';
import { Tag } from './tag.model';

export class PostCover{
    _id: string;
    title: string;
    description: string;
    writeDate?: Date;
    creationDate: Date = new Date();
    creator: User = new User();
    hasFeaturePicture: boolean = false;
    isActive: boolean = false;
    tags: Tag[] = [];
    postId: string;

    constructor(data:any = null){
        autoBind(this);

        if(data != null){
            this._id = data._id;
            this.title = data.title;
            this.description = data.description;
            this.creationDate = new Date(data.creationDate);
            this.creator = new User(data.creator);
            this.hasFeaturePicture = data.hasFeaturePicture;
            this.isActive = data.isActive;

            if(data.writeDate != null || data.writeDate != undefined){
                this.writeDate = new Date(data.writeDate);
            }

            if (data.tags){
                this.tags = data.tags.map(function(tag:any){ return new Tag(tag); });
            }
        }
    }
}