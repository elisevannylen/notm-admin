import * as autoBind from 'auto-bind';
import { PostCover } from './postCover.model';
import { Picture } from './picture.model';
import { Tag } from './tag.model';

export class Post{
    _id: string;
    details: PostCover = new PostCover();
    text: string = "";
    pictures: Picture[] = [];
    tags: Tag[] = [];

    constructor(data:any = null){
        autoBind(this);

        if(data != null){
            this._id = data._id;
            this.details = new PostCover(data.details);
            this.text = data.text;

            if (data.pictures){
                this.pictures = data.pictures.map(function(picture:any){ return new Picture(picture); });
            }

            if (data.tags){
                this.tags = data.tags.map(function(tag:any){ return new Tag(tag); });
            }
        }
    }
}