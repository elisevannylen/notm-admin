import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaterialModule } from '../material-module';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { FormsModule } from '@angular/forms';
import { TagsRoutingModule } from './tags-routing.module';

import { TagService } from './tag.service';
import { TagListComponent } from './tag-list/tag-list.component';
import { TagDetailComponent } from './tag-detail/tag-detail.component';


@NgModule({
  declarations: [TagListComponent, TagDetailComponent],
  imports: [
    CommonModule,
    MaterialModule,
    AngularFontAwesomeModule,
    FormsModule,
    TagsRoutingModule
  ],
  providers: [
    TagService
  ]
})
export class TagsModule { }
