import { Component, OnInit } from '@angular/core';
import { TagService } from '../tag.service';
import { Tag } from '../../models/tag.model';
import { NotificationHelper } from '../../helpers/notificationHelper';

@Component({
	selector: 'app-tag-list',
	templateUrl: './tag-list.component.html',
	styleUrls: ['./tag-list.component.css', './tag-list.colums.css']
})
export class TagListComponent implements OnInit {
	displayedColumns = ['name', 'actions'];
	tags: Tag[] = [];

  	constructor(private tagService:TagService, private notificationHelper:NotificationHelper) { }

	async ngOnInit() {
		await this.getTags();
	}

	async getTags(){
		this.tags = await this.tagService.getTags();
	}

	async onDelete(tagId:string):Promise<void> {
		let usageInPosts = await this.tagService.getUsageTagInPosts(tagId);
		let tag = await this.tagService.getTag(tagId);

		if(usageInPosts == 0){
			await this.tagService.deleteTag(tagId);
			await this.getTags();
			this.notificationHelper.openUndoSnackBar("Tag '" + tag.name + "' is deleted.", async () => {
				await this.tagService.createTag(tag);
				await this.getTags();
			});
		} else {
			this.notificationHelper.openSnackBar("Tag '" + tag.name + "' is used " + usageInPosts + " times.", "Ok");
		}
	}
}
