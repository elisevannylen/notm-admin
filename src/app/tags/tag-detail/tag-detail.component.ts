import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TagService } from '../tag.service';
import { Tag } from '../../models/tag.model';

@Component({
  selector: 'app-tag-detail',
  templateUrl: './tag-detail.component.html',
  styleUrls: ['./tag-detail.component.css']
})
export class TagDetailComponent implements OnInit, OnDestroy {
	private sub: any;
	tagId: string;
	tag:Tag = new Tag();
	title: string = "Add tag";

  	constructor(private router: Router, private route: ActivatedRoute, private tagService:TagService) { }

  	async ngOnInit(): Promise<void> {
		this.sub = this.route.params.subscribe(params => {this.tagId = params['id'];});
		if(this.tagId != "0"){
			this.tag = await this.tagService.getTag(this.tagId);
			this.title = "Update tag";
		}
  	}

	ngOnDestroy(): void {
		this.sub.unsubscribe();
	}

	async onSaveTag(){
		if(this.tagId == "0"){
			await this.tagService.createTag(this.tag);
		} else {
			await this.tagService.updateTag(this.tag);
		}

		this.router.navigate(['/tags']);
	}
}