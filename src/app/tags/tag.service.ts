import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Tag } from '../models/tag.model';
import Config from '../config';

@Injectable()
export class TagService {
    constructor(private http: HttpClient) {}

    async getTags():Promise<Array<Tag>>{
        let results = await this.http.get(Config.basesite + "/tags").toPromise();

        let tags:Array<Tag> = [];
        for(let i in results){
            let tag = new Tag(results[i]);
            tags.push(tag);
        }

        let sortedTags:Array<Tag> = tags.sort((a,b) => {
            return (a.name > b.name) ? 1 : ((a.name < b.name) ? -1 : 0);
        });

        return sortedTags;
    }

    async getTag(tagId:string):Promise<Tag>{
        let result = await this.http.get(Config.basesite + "/tags/" + tagId).toPromise();
        return new Tag(result);
    }

    async getUsageTagInPosts(tagId:string):Promise<number>{
		let results = await this.http.get(Config.basesite + "/posts/byTag/" + tagId).toPromise();
        let usage: number = 0;
        
		for (let i in results){
			usage++;
		}
		return usage;
	}

    async createTag(tag:Tag):Promise<Tag>{
        let result = await this.http.post(Config.basesite + "/tags", tag).toPromise();
        return new Tag(result);
    }

    async updateTag(tag:Tag):Promise<Tag>{
        let result = await this.http.put(Config.basesite + "/tags/" + tag._id, tag).toPromise();
        return new Tag(result);
    }

    async deleteTag(tagId:string):Promise<any>{
        let result = await this.http.delete(Config.basesite + "/tags/" + tagId).toPromise();
    }
}