import {Injectable} from "@angular/core";
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable()
export class NotificationHelper {
    
    constructor(private snackBar: MatSnackBar) { }

    openUndoSnackBar(message: string, undoFunction: Function, duration = 4000) {
        const snackBarRef = this.snackBar.open(message, "Undo", { duration: duration });
        
        snackBarRef.onAction().subscribe(() => {
			undoFunction();
		});
    }
    
    openSnackBar(message: string, action: string, duration = 2000, actionFunction?: Function) {
        const snackBarRef = this.snackBar.open(message, action, { duration: duration });
        
        if(actionFunction != undefined){
            snackBarRef.onAction().subscribe(() => {
                actionFunction();
            });
        }
	}
}