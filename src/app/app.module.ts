import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from "@angular/common/http";
import { HttpModule } from "@angular/http";
import { FormsModule } from '@angular/forms';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { MaterialModule } from './material-module';
import { PostsModule } from './posts/posts.module';
import { TagsModule } from './tags/tags.module';
import { UsersModule } from './users/users.module';

import { httpInterceptorProviders } from './http-interceptors/httpInterceptorProviders';
import { AuthService } from './auth/auth.service';
import { AuthGuardService } from './auth/auth-guard.service';
import { JwtHelperService, JWT_OPTIONS } from '@auth0/angular-jwt';
import { NotificationHelper } from './helpers/notificationHelper';

import { LayoutComponent } from './components/layout/layout.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { SideNavComponent } from './components/side-nav/side-nav.component';
import { LoginComponent } from './components/login/login.component';
import { LoginDialogComponent } from './components/login/login-dialog/login-dialog.component';

@NgModule({
	entryComponents: [LoginDialogComponent],
	declarations: [
		AppComponent,
		LayoutComponent,
		ToolbarComponent,
		SideNavComponent,
		LoginComponent,
		LoginDialogComponent
	],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		HttpModule,
		HttpClientModule,
		FormsModule,
		AngularFontAwesomeModule,
		MaterialModule,
		PostsModule,
		TagsModule,
		UsersModule,
		AppRoutingModule
	],
	providers: [
		httpInterceptorProviders,
		AuthService,
		AuthGuardService,
		JwtHelperService,
		{ provide: JWT_OPTIONS, useValue: JWT_OPTIONS },
		{ provide: MAT_DIALOG_DATA, useValue: {} },
		{ provide: MatDialogRef, useValue: {} },
		NotificationHelper
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
